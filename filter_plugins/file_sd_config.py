import string

def to_scrape_config(groups, deny_groups, group_names, hostvars, pattern):
    """
    build a prometheus file_sd_config from groups

    For example

    groups = {
      'all': ['meet.yolo.wtf', 'bbbackend01.sft.mx', 'bbbackend02.sft.mx', 'nextcloud.sft.mx'],
      'nextcloud_standalone': ['nextcloud.sft.mx'],
      'bbbackend_n0emis': ['bbbackend01.sft.mx', 'bbbackend02.sft.mx'],
      'bbb_standalone': ['meet.yolo.wtf'],
      'bbb_n0emis': ['meet.yolo.wtf', 'bbbackend01.sft.mx', 'bbbackend02.sft.mx'],
      'nextcloud': ['nextcloud.sft.mx'],
    }

    and

    deny_groups = ['all', 'bbb_n0emis', 'nextcloud']

    should result in

    [
        {
            'targets': [
                'meet.yolo.wtf'
            ],
            'labels': {'group': 'bbb_standalone'}
        },
        {
            'targets': [
                'bbbackend01.sft.mx',
                'bbbackend02.sft.mx'
            ],
            'labels': {'group': 'bbbackend_n0emis'}
        }
    ]
    """

    def get_candidates(groups, group_names):
        candidates = set()
        for group_name in group_names:
            exclude = False
            if group_name.startswith('!'):
                group_name = group_name[1:]
                exclude = True

            try:
                members = set(groups[group_name])
            except KeyError:
                print(f"Idiot: {group_name}")
                continue

            if exclude:
                candidates -= members
            else:
                candidates |= members
        return sorted(candidates)

    filtered = {g: [m for m in ms if m in get_candidates(groups, group_names)] for g, ms in groups.items() if g not in deny_groups}

    template = string.Template(pattern)

    conf = []
    for group, members in filtered.items():
        if not members:
            continue

        targets = set()
        for member in members:
            targets.add(template.substitute(hostvars[member]))

        conf.append({'targets': sorted(targets), 'labels': {'group': group}})

    return conf


class FilterModule():
    def filters(self):
        return {'to_scrape_config': to_scrape_config}
